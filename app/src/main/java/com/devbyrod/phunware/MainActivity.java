package com.devbyrod.phunware;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.devbyrod.phunware.application.Phunware;
import com.devbyrod.phunware.fragments.ArticleFragment;
import com.devbyrod.phunware.fragments.HeadlinesFragment;
import com.devbyrod.phunware.models.Venue;


public class MainActivity extends ActionBarActivity implements HeadlinesFragment.HeaderlinesFragmentItemListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if( findViewById( R.id.fragment_container ) != null ){

            if( savedInstanceState != null ){

                return;
            }

            getSupportFragmentManager()
                    .beginTransaction()
                    .add( R.id.fragment_container, new HeadlinesFragment(), HeadlinesFragment.TAG )
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if( id == android.R.id.home ){

            getSupportFragmentManager().popBackStack();
            this.setUpButton( false );
            return true;
        }

        if (id == R.id.action_share) {

            this.shareVenueInfo();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked( int venueIndex ) {

        ArticleFragment fragment = (ArticleFragment) getSupportFragmentManager().findFragmentById( R.id.article_fragment );

        if( fragment != null ){

            fragment.loadVenueInfo( Phunware.getCurrentVenue() );
            return;
        }

        //small screen (phones)
        fragment = new ArticleFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace( R.id.fragment_container, fragment )
                .addToBackStack( null )
                .commit();

        this.setUpButton( true );
    }

    private void shareVenueInfo(){

        Venue venue = Phunware.getCurrentVenue();

        if( venue == null ){

            Toast.makeText( this, R.string.error_select_venue, Toast.LENGTH_LONG ).show();
            return;
        }

        String textToShare = venue.getName() + "\n" + venue.getAddress() + ". " + venue.getCity() + ", " + venue.getState() ;

        Intent intent = new Intent( Intent.ACTION_SEND );
        intent.setType( "text/plain" );
        intent.putExtra( Intent.EXTRA_SUBJECT, "Phunware Sharing" );
        intent.putExtra( Intent.EXTRA_TEXT, textToShare );
        startActivity( Intent.createChooser( intent, getString( R.string.share_text ) ) );
    }

    private void setUpButton( boolean value ){

        getSupportActionBar().setDisplayHomeAsUpEnabled( value );
    }
}
