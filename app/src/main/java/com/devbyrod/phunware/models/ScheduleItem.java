package com.devbyrod.phunware.models;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mandrake on 4/27/15.
 */

public class ScheduleItem {
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("EEEE M/d");
    private static final SimpleDateFormat TIME_FORMATTER = new SimpleDateFormat("h:mma");

    @SerializedName("start_date")
    private String mStrStartDate;

    @SerializedName("end_date")
    private String mStrEndDate;

    private Date mStartDate;
    private Date mEndDate;


    public Date getStartDate() {
        return mStartDate;
    }

    public void setStartDate(String mStartDate) throws ParseException {
        this.mStrStartDate = mStartDate;
        this.mStartDate = FORMATTER.parse( this.mStrStartDate );
    }

    public Date getEndDate() {
        return mEndDate;
    }

    public void setEndDate(String mEndDate) throws ParseException {
        this.mStrEndDate = mEndDate;
        this.mEndDate = FORMATTER.parse( this.mStrEndDate );
    }

    public String getStartDateString() {

        if( this.mStartDate == null ) {

            try {
                this.mStartDate = FORMATTER.parse(this.mStrStartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return DATE_FORMATTER.format(mStartDate);
    }

    public String getStartTimeString() {

        if( this.mStartDate == null ) {

            try {
                this.mStartDate = FORMATTER.parse(this.mStrStartDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return TIME_FORMATTER.format(mStartDate);
    }

    public String getEndDateString() {


        if (this.mEndDate == null) {

            try {
                this.mEndDate = FORMATTER.parse(this.mStrEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return DATE_FORMATTER.format(mEndDate);
    }

    public String getEndTimeString() {


        if( this.mEndDate == null ) {

            try {
                this.mEndDate = FORMATTER.parse(this.mStrEndDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return TIME_FORMATTER.format(mEndDate);
    }

    @Override
    public boolean equals(Object o) {
        boolean result = false;
        if (o instanceof ScheduleItem) {
            result = mStartDate.equals(((ScheduleItem) o).getStartDate())
                    && mEndDate.equals(((ScheduleItem) o).getEndDate());
        }
        return result;
    }

    @Override
    public int hashCode() {
        String s = getStartDateString() + getEndDateString();
        return s.hashCode();
    }

}
