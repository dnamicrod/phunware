package com.devbyrod.phunware.application;

import android.app.Application;

import com.devbyrod.phunware.models.Venue;

/**
 * Created by Mandrake on 4/27/15.
 */
public class Phunware extends Application {

    private static Venue mCurrentVenue;

    public static Venue getCurrentVenue() {

        return mCurrentVenue;
    }

    public static void setCurrentVenue(Venue mCurrentVenue) {

        Phunware.mCurrentVenue = mCurrentVenue;
    }
}
