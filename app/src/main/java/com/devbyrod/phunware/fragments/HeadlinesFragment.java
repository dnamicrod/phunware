package com.devbyrod.phunware.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.devbyrod.phunware.R;
import com.devbyrod.phunware.adapters.HeadlinesAdapter;
import com.devbyrod.phunware.application.Phunware;
import com.devbyrod.phunware.models.Venue;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class HeadlinesFragment extends Fragment implements AdapterView.OnItemClickListener {

    public final static String TAG = HeadlinesFragment.class.getSimpleName();
    public final static String URL = "https://s3.amazonaws.com/jon-hancock-phunware/nflapi-static.json";

    private HeaderlinesFragmentItemListener mListener;
    private ListView mListView;
    private List<Venue> mVenues = new ArrayList<>();

    public HeadlinesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_headlines, container, false);

        mListView = (ListView) view.findViewById( R.id.list_venues );
        mListView.setOnItemClickListener( this );

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.loadVenues();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mListener = (HeaderlinesFragmentItemListener) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    private void loadVenues(){

        Ion.with(getActivity().getApplicationContext())
                .load(HeadlinesFragment.URL)
                .as(new TypeToken<List<Venue>>() {})
                .setCallback(new FutureCallback<List<Venue>>() {
                    @Override
                    public void onCompleted(Exception e, List<Venue> result) {

                        mVenues = new ArrayList<>( result );
                        mListView.setAdapter( new HeadlinesAdapter( getActivity().getApplicationContext(), 0, mVenues ) );
                    }
                });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if( mListener != null ){

            Phunware.setCurrentVenue( mVenues.get( position ) );
            mListener.onItemClicked( position );
        }
    }

    public interface HeaderlinesFragmentItemListener {

        public void onItemClicked( int venueIndex );
    }
}
