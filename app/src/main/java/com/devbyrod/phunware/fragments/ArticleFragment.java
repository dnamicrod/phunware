package com.devbyrod.phunware.fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devbyrod.phunware.R;
import com.devbyrod.phunware.application.Phunware;
import com.devbyrod.phunware.models.ScheduleItem;
import com.devbyrod.phunware.models.Venue;
import com.koushikdutta.ion.Ion;

import org.w3c.dom.Text;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArticleFragment extends Fragment {

    public final static String VENUE_INDEX = "venue_index";

    private View mContentView;
    private ImageView mImgVenueImage;
    private TextView mTxtVenueName;
    private TextView mTxtVenueAddress;

    public ArticleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContentView =  inflater.inflate(R.layout.fragment_article, container, false);

        this.mImgVenueImage = (ImageView) this.mContentView.findViewById( R.id.img_venue );
        this.mTxtVenueName = (TextView) this.mContentView.findViewById( R.id.txt_venue_name );
        this.mTxtVenueAddress = (TextView) this.mContentView.findViewById( R.id.txt_venue_address );

        return this.mContentView;
    }

    @Override
    public void onStart() {

        super.onStart();

        Venue venue = Phunware.getCurrentVenue();

        if( venue != null ){

            this.loadVenueInfo( venue );
        }
    }

    public void loadVenueInfo( Venue venue ){

        String url = venue.getImageUrl();

        if( url != null && !url.isEmpty() ){

            Ion.with( this.mImgVenueImage )
                    .load( url );

            this.mImgVenueImage.setVisibility(View.VISIBLE);
        }
        else{

            this.mImgVenueImage.setVisibility( View.GONE );
        }

        this.mTxtVenueName.setText( venue.getName() );
        this.mTxtVenueAddress.setText( venue.getAddress() + "\n" + venue.getCity() + ", " + venue.getState() + " " + venue.getZip() );

        this.loadVenueSchedule( venue );
    }

    private void loadVenueSchedule( Venue venue ){

        List<ScheduleItem> schedule = venue.getSchedule();

        if( schedule == null || schedule.isEmpty() ){

            return;
        }

        TextView textView;

        for( ScheduleItem item : schedule ){

            textView = new TextView( getActivity().getApplicationContext() );
            textView.setText( item.getStartDateString() + " " + item.getStartTimeString().replace("AM","am").replace("PM","pm")
                    + " to " + item.getEndTimeString().replace("AM","am").replace("PM","pm") );
            textView.setTextColor( getResources().getColor( R.color.venue_item_address ) );
            textView.setPadding( 0, 0, 0, 18 );
            ( (ViewGroup)this.mContentView ).addView(textView);
        }
    }
}
