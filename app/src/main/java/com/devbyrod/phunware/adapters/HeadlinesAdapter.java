package com.devbyrod.phunware.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.devbyrod.phunware.R;
import com.devbyrod.phunware.models.Venue;

import java.util.List;

/**
 * Created by Mandrake on 4/27/15.
 */
public class HeadlinesAdapter extends ArrayAdapter<Venue> {

    LayoutInflater mInflater;

    public HeadlinesAdapter(Context context, int resource, List<Venue> objects) {

        super(context, resource, objects);

        mInflater = LayoutInflater.from( context );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Viewholder holder;

        if( convertView == null ){

            convertView = mInflater.inflate( R.layout.list_item_venue, null );

            holder = new Viewholder();

            holder.txtVenueName = (TextView) convertView.findViewById( R.id.txt_venue_name );
            holder.txtVenueAddress = (TextView) convertView.findViewById( R.id.txt_venue_address );

            convertView.setTag( holder );
        }
        else{

            holder = (Viewholder) convertView.getTag();
        }

        Venue venue = getItem( position );

        holder.txtVenueName.setText( venue.getName() );
        holder.txtVenueAddress.setText( venue.getAddress() + "." + venue.getCity() + ", " + venue.getState() );

        return convertView;
    }

    private static class Viewholder{

        TextView txtVenueName;
        TextView txtVenueAddress;
    }
}
